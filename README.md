# Data Explorer

A simple getting started `docker-compose` setup for using using `Apache Superset`.


## Local Development

A `Makefile` can be used to handle any operations in the repo.

```
Usage:
    help:    Prints this screen
    build:   Build images
    run:     Runs the data explorer
    init-db: Initalize the superset DB
```
