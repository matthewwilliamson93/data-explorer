.PHONY: help
help:
	@echo "Usage:"
	@echo "    help:          Prints this screen"
	@echo "    update-images: Updates the base images"
	@echo "    build:         Build images"
	@echo "    run:           Runs the data explorer"
	@echo "    init-db:       Initalize the superset DB"
	@echo ""

.PHONY: update-images
update-images:
	docker-compose pull
	docker-compose build --pull

.PHONY: build
build:
	docker-compose build

.PHONY: run
run:
	docker-compose up

.PHONY: init-db
init-db:
	docker exec -it data-explorer-superset superset-init
