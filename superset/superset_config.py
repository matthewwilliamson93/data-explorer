import os

hostname = "postgres"
port = os.environ["POSTGRES_PORT"]
database = os.environ["POSTGRES_DB"]

user = os.environ["POSTGRES_USER"]
password = os.environ["POSTGRES_DB"]

SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{user}:{password}@{hostname}:{port}/{database}"

PUBLIC_ROLE_LIKE_GAMMA = True

SECRET_KEY = "Show me what you got"
